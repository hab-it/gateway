package com.example.habitgateway.config;

import com.example.habitgateway.filter.AuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {
    @Autowired
    AuthenticationFilter filter;

    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r
                        .path("/habit/**")
                        .filters(f -> f.filter(filter))
                        .uri("lb://habit-service"))
                .route(r -> r
                        .path("/user/**")
                        .filters(f -> f.filter(filter))
                        .uri("lb://user-service"))
                .route(r -> r
                        .path("/score/**")
                        .filters(f -> f.filter(filter))
                        .uri("lb://score-service"))
                .build();
    }
}
