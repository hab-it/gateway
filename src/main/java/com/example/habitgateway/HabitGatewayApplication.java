package com.example.habitgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class HabitGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(HabitGatewayApplication.class, args);
	}

}
